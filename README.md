This repository contains the code for running the analyses presented in the manuscript
"Tau pathology mediates the effects of amyloid on neocortical tau propagation and neurodegeneration among non-demented individuals"
by  Murat Bilgel, Dean F. Wong, Abhay R. Moghekar, Luigi Ferrucci, and Susan M. Resnick.
After running the `Rnw` file, a `tex` file will be generated. 
Compiling this `tex` file will generate the `pdf` that corresponds to the preprint version of this manuscript.

# Running the analyses & compiling the manuscript

To be able to run the container and the following analyses, you need to:

1. Obtain the necessary input data by making a request on the
BLSA website.
2. Download this repository:
    ```git clone git@gitlab.com:bilgelm/tau_amyloid_neurodegeneration.git```
3. Put the data files obtained in step 1 into a directory named `data/` at the root of the git repository obtained in step 2.
4. Compile the following `tex` files:
    ```
    cd paper/
    pdflatex blsa_dags_row1.tex
    pdflatex blsa_dags_row2.tex
    pdflatex blsa_dags_row3.tex
    pdflatex dag.tex
    ```

5. Run all statistical analyses and compile manuscript (still in the `paper/` directory):
    ```
    Rscript -e "library(knitr); knit('manuscript.Rnw')"
    pdflatex manuscript
    bibtex manuscript
    pdflatex manuscript
    pdflatex manuscript
    ```
