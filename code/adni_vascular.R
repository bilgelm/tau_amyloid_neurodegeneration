library(tidyverse)
library(units)
library(lubridate)

#repopath <- '/Volumes/niairp2/LBN/BABS/cogstore/cog1/murat/tau_cbf'
repopath <- system("git rev-parse --show-toplevel", intern=T)

## ---- adni_vascular
adni.dir <- file.path(repopath,'data','adni')

adni.demog.fname <- file.path(adni.dir,'PTDEMOG.csv')

adni.vitals.fname <- file.path(adni.dir,'VITALS.csv')
adni.av45vitals.fname <- file.path(adni.dir,'AV45VITALS.csv')

adni.petmeta1.fname <- file.path(adni.dir,'PETMETA_ADNI1.csv')
adni.petmeta2.fname <- file.path(adni.dir,'PETMETA_ADNIGO2.csv')
adni.petmeta3.fname <- file.path(adni.dir,'PETMETA3.csv')

adni.lipids.fname <- file.path(adni.dir,'ADNINIGHTINGALELONG_05_24_21.csv')

adni.medhist.fname <- file.path(adni.dir,'MEDHIST.csv')
adni.recmhist.fname <- file.path(adni.dir,'RECMHIST.csv')
adni.reccmeds.fname <- file.path(adni.dir,'RECCMEDS.csv')

adni.modhach.fname <- file.path(adni.dir,'MODHACH.csv')

adni.demog <- read_csv(adni.demog.fname,
                       col_types = cols_only(
                         RID = col_integer(),
                         PTGENDER = col_integer(),
                         PTDOBYY = col_double() # we just use DOB year (to compute age for 10-yr CVD risk score) to keep things simpler
                       ),
                       na = c("", "NA", -4)) %>%
  drop_na() %>%
  distinct(.keep_all=T) %>% # keep one row per RID
  mutate(sex = case_when(PTGENDER==1 ~ "Male",
                         PTGENDER==2 ~ "Female",
                         T ~ NA_character_)) %>%
  dplyr::select(RID, PTDOBYY, sex) %>%
  arrange(RID)


# VSWEIGHT, VSWTUNIT: 1=pounds; 2=kilograms
# VSHEIGHT, VSHTUNIT: 1=inches; 2=centimeters

# sanity checks
# approx. min and max human height: 21-85 in (54-215 cm)
# weights seem sensible (there is one outlier: 493 kg, but still possible), so we don't check

adni.vitals <- read_csv(adni.vitals.fname, na = c("", "NA", -4, -1)) %>%
  filter(!(VISCODE2 %in% c("f", "uns1"))) %>%
  filter(!(VISCODE %in% c("f", "uns1"))) %>%
  mutate(weight = case_when(VSWTUNIT==1 ~ set_units(set_units(VSWEIGHT, "lb"), "kg"),
                            VSWTUNIT==2 ~ set_units(VSWEIGHT, "kg")),
         height = case_when((VSHTUNIT==1) & (VSHEIGHT>21) & (VSHEIGHT<85) ~ set_units(set_units(VSHEIGHT, "in"), "m"), # unit correctly coded as in
                            (VSHTUNIT==1) & (VSHEIGHT>=85) & (VSHEIGHT<215) ~ set_units(set_units(VSHEIGHT, "cm"), "m"), # unit incorrectly coded as in; should be cm
                            (VSHTUNIT==2) & (VSHEIGHT>54) & (VSHEIGHT<215) ~ set_units(set_units(VSHEIGHT, "cm"), "m"), # unit correctly coded as cm
                            (VSHTUNIT==2) & (VSHEIGHT>21) & (VSHEIGHT<=54) ~ set_units(set_units(VSHEIGHT, "in"), "m")), # unit incorrectly coded as cm; should be in; this doesn't happen in this dataset
         bmi = weight / height^2,
         sbp = replace(VSBPSYS, VSBPSYS<1, NA), # in mmHg
         dbp = replace(VSBPDIA, VSBPDIA<1, NA), # in mmHg
         examyear = case_when(!is.na(EXAMDATE) ~ year(EXAMDATE), # we just use visit year for matching to keep things simpler
                              !is.na(USERDATE) ~ year(USERDATE),
                              !is.na(USERDATE2) ~ year(USERDATE2))) %>%
  dplyr::select(RID, examyear, bmi, sbp, dbp) %>%
  drop_na(RID) %>%
  filter(!(is.na(bmi) & is.na(sbp) & is.na(dbp))) %>% # at least one of (bmi, sbp, dbp) should be present to retain row
  arrange(RID, examyear) %>%
  group_by(RID, examyear) %>%
  fill(bmi, sbp, dbp, .direction="downup") %>% # in case there are multiple rows of the same RID and exam year, impute any missing values by propagating "closest" available value
  ungroup() %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

adni.av45vitals <- read_csv(adni.av45vitals.fname, na = c("", "NA", -4, -1)) %>%
  drop_na(RID, SCANDATE) %>%
  mutate(examyear = year(SCANDATE)) %>% # we just use visit year for matching to keep things simpler
  dplyr::select(RID, examyear, 
                PRESYSTBP, PREDIABP, POSTSYSTBP, POSTDIABP) %>%
  filter(!(is.na(PRESYSTBP) & is.na(PREDIABP) & is.na(POSTSYSTBP) & is.na(POSTDIABP))) %>% # at least one of (PRESYSTBP, PREDIABP, POSTSYSTBP, POSTDIABP) should be present to retain row
  arrange(RID, examyear) %>%
  group_by(RID, examyear) %>%
  fill(PRESYSTBP, PREDIABP, POSTSYSTBP, POSTDIABP, .direction="downup") %>% # in case there are multiple rows of the same RID and exam year, impute any missing values by propagating "closest" available value
  ungroup() %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

# PMBLGLUC -there is one outlier: 250. Several entries < 70
# assuming unit is mg/dL
adni.petmeta <- read_csv(adni.petmeta1.fname, na = c("", "NA", -4, -1)) %>%
  dplyr::select(RID, EXAMDATE, USERDATE, USERDATE2, PMBLGLUC) %>%
  bind_rows(read_csv(adni.petmeta2.fname, na = c("", "NA", -4)) %>%
              dplyr::select(RID, EXAMDATE, USERDATE, USERDATE2, PMBLGLUC)) %>%
  bind_rows(read_csv(adni.petmeta3.fname, na = c("", "NA", -4)) %>%
              dplyr::select(RID, SCANDATE, USERDATE, USERDATE2, PMBLGLUC) %>%
              rename(EXAMDATE = SCANDATE)) %>%
  drop_na(RID, PMBLGLUC) %>%
  mutate(examyear = case_when(!is.na(EXAMDATE) ~ year(EXAMDATE), # we just use visit year for matching to keep things simpler
                              !is.na(USERDATE) ~ year(USERDATE),
                              !is.na(USERDATE2) ~ year(USERDATE2))) %>%
  dplyr::select(-EXAMDATE, -USERDATE, -USERDATE2) %>%
  arrange(RID, examyear) %>%
  group_by(RID, examyear) %>%
  fill(PMBLGLUC, .direction="downup") %>% # in case there are multiple rows of the same RID and exam year, impute any missing values by propagating "closest" available value
  ungroup() %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

# values in csv file are in mmol/L
# to convert total, HDL, LDL cholesterol from mmol/L to mg/dL, multiply by 38.67
# to convert total triglycerides         from mmol/L to mg/dL, multiply by 88.57
adni.lipids <- read_csv(adni.lipids.fname, na = c("", "NA", -4, -1)) %>%
  mutate(examyear = year(EXAMDATE)) %>%
  dplyr::select(RID, examyear, TOTAL_C, LDL_C, HDL_C, TOTAL_TG) %>%
  mutate(across(.cols=c(TOTAL_C, LDL_C, HDL_C), .fns = function(x) x * 38.67)) %>%
  mutate(TOTAL_TG = TOTAL_TG * 88.57) %>%
  drop_na(RID) %>%
  filter(!(is.na(TOTAL_C) & is.na(LDL_C) & is.na(HDL_C) & is.na(TOTAL_TG))) %>% # at least one of (TOTAL_C, LDL_C, HDL_C, TOTAL_TG) should be present to retain row
  arrange(RID, examyear) %>%
  group_by(RID, examyear) %>%
  fill(TOTAL_C, LDL_C, HDL_C, TOTAL_TG, .direction="downup") %>% # in case there are multiple rows of the same RID and exam year, impute any missing values by propagating "closest" available value
  ungroup() %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

htn_base_regex_str = '(?:HTN|hy[op]ertension|hpertension|elevated blood pressure|elevated b[/ ]?p|high blood pressure|high b[/ ]?p)'
htn_regex_str = '(?<!no )(?<!borderline )(?<!intermittent )(?<!\'borderline\' )(?:HTN|hy[op]ertension|elevated blood pressure|elevated b[/ ]?p|high blood pressure|high b[/ ]?p)(?!\\s*-?\\s*borderline)'
diabetes_base_regex_str = 'diabet\\w+'
diabetes_regex_str = '(?<!pre)(?<!pre )(?<!pre-)(?<!no )(?<!not )(?<! not diagnosed )(?<!borderline )(?<!\'borderline\' )(type 2 )?diabet\\w+(?!\\s*-?\\s*borderline)'

adni.medhist <- read_csv(adni.medhist.fname, na = c("", "NA", -4, -1)) %>%
  drop_na(RID) %>%
  mutate(examyear = case_when(!is.na(EXAMDATE) ~ year(EXAMDATE), # we just use visit year for matching to keep things simpler
                              !is.na(USERDATE) ~ year(USERDATE),
                              !is.na(USERDATE2) ~ year(USERDATE2))) %>%
  dplyr::select(RID, examyear, MH14ALCH, MH16SMOK, MHCOMMEN) %>%
  filter(!(is.na(MH14ALCH) & is.na(MH16SMOK) & is.na(MHCOMMEN))) %>%
  group_by(RID, examyear) %>% 
  mutate(MHCOMMEN = case_when(all(is.na(MHCOMMEN)) ~ NA_character_,
                              T ~ paste0(MHCOMMEN[!is.na(MHCOMMEN)], collapse=" "))) %>% # concatenate all text comments within a participant-year
  ungroup() %>%
  distinct(.keep_all=T) %>% # remove duplicate rows
  mutate(htn.medhist = case_when(is.na(MHCOMMEN) ~ NA_integer_,
                                 grepl("^none\\.?$", MHCOMMEN, ignore.case=T) ~ 0L,
                                 grepl(htn_regex_str, MHCOMMEN, ignore.case=T, perl=T) ~ 1L,
                                 grepl(htn_base_regex_str, MHCOMMEN, ignore.case=T, perl=T) ~ 0L),
         diabetes.medhist = case_when(is.na(MHCOMMEN) ~ NA_integer_,
                                      grepl("^none\\.?$", MHCOMMEN, ignore.case=T) ~ 0L,
                                      grepl(diabetes_regex_str, MHCOMMEN, ignore.case=T, perl=T) ~ 1L,
                                      grepl(diabetes_base_regex_str, MHCOMMEN, ignore.case=T, perl=T) ~ 0L),
         smoking.medhist = case_when(is.na(MHCOMMEN) ~ NA_integer_,
                                     grepl("^none\\.?$", MHCOMMEN, ignore.case=T) ~ 0L,
                                     grepl('smok.+', MHCOMMEN, ignore.case=T, perl=T) ~ 1L,
                                     grepl('cigar.*', MHCOMMEN, ignore.case=T, perl=T) ~ 1L)) %>%
  rename(smoking = MH16SMOK,
         alcohol_abuse = MH14ALCH) %>% 
  arrange(RID, examyear) %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

adni.recmhist <- read_csv(adni.recmhist.fname, na = c("", "NA", -4, -1)) %>%
  drop_na(RID, MHDESC) %>%
  mutate(examyear = case_when(!is.na(EXAMDATE) ~ year(EXAMDATE), # we just use visit year for matching to keep things simpler
                              !is.na(USERDATE) ~ year(USERDATE),
                              !is.na(USERDATE2) ~ year(USERDATE2))) %>%
  dplyr::select(RID, examyear, MHDESC) %>%
  group_by(RID, examyear) %>%
  mutate(MHDESC = paste0(MHDESC, collapse=" ")) %>% # concatenate all text comments within a participant-year
  ungroup() %>%
  distinct(.keep_all=T) %>% # remove duplicate rows
  mutate(htn.recmhist = case_when(grepl("^none\\.?$", MHDESC, ignore.case=T) ~ 0L,
                                  grepl(htn_regex_str, MHDESC, ignore.case=T, perl=T) ~ 1L,
                                  grepl(htn_base_regex_str, MHDESC, ignore.case=T, perl=T) ~ 0L),
         diabetes.recmhist = case_when(grepl("^none\\.?$", MHDESC, ignore.case=T) ~ 0L,
                                       grepl(diabetes_regex_str, MHDESC, ignore.case=T, perl=T) ~ 1L,
                                       grepl(diabetes_base_regex_str, MHDESC, ignore.case=T, perl=T) ~ 0L),
         smoking.recmhist = case_when(grepl("^none\\.?$", MHDESC, ignore.case=T) ~ 0L,
                                      grepl('smok.+', MHDESC, ignore.case=T, perl=T) ~ 1L,
                                      grepl('cigar.*', MHDESC, ignore.case=T, perl=T) ~ 1L)) %>%
  arrange(RID, examyear) %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

htn_medlist <- tolower(read_csv(file.path(repopath,'code','htn_medlist.txt'), 
                          col_names=F, col_types="c", 
                          comment='#', skip_empty_rows = T)$X1)

adni.reccmeds <- read_csv(adni.reccmeds.fname, na = c("", "NA", -4, -1)) %>%
  drop_na(RID) %>%
  mutate(examyear = case_when(!is.na(EXAMDATE) ~ as.numeric(str_sub(EXAMDATE, -4, -1)), # we just use visit year for matching to keep things simpler
                              !is.na(USERDATE) ~ year(USERDATE),
                              !is.na(USERDATE2) ~ year(USERDATE2))) %>%
  dplyr::select(RID, examyear, CMMED, CMREASON) %>%
  filter(!(is.na(CMMED) & is.na(CMREASON))) %>%
  group_by(RID, examyear) %>%
  mutate(CMMED = paste0(CMMED[!is.na(CMMED)], collapse= " "), # concatenate all text comments within a participant-year
         CMREASON = paste0(CMREASON[!is.na(CMREASON)], collapse = " ")) %>%
  ungroup() %>%
  distinct(.keep_all=T) %>% # remove duplicate rows
  mutate(htn.reccmeds = case_when(grepl(htn_regex_str, CMREASON, ignore.case=T, perl=T) ~ 1L),
         diabetes.reccmeds = case_when(grepl(diabetes_regex_str, CMREASON, ignore.case=T, perl=T) ~ 1L)) %>%
  group_by(RID, examyear) %>%
  mutate(htn.treatment = case_when(htn.reccmeds==1 ~ 1L,
                                   any(CMMED %in% htn_medlist) ~ 1L)) %>% # verified: htn.treatment and htn.reccmeds are the same
  arrange(RID, examyear) %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

adni.modhach <- read_csv(adni.modhach.fname, na = c("", "NA", -4, -1)) %>%
  drop_na(RID) %>%
  filter(!(is.na(HMHYPERT) & is.na(HMSTROKE) & is.na(HMSCORE))) %>%
  mutate(examyear = case_when(!is.na(EXAMDATE) ~ year(EXAMDATE),  # we just use visit year for matching to keep things simpler
                              !is.na(USERDATE) ~ year(USERDATE),
                              !is.na(USERDATE2) ~ year(USERDATE2))) %>%
  dplyr::select(RID, examyear, HMHYPERT, HMSTROKE, HMSCORE) %>% # at least one of (HMHYPERT, HMSTROKE, HMSCORE) should be present to retain row
  arrange(RID, examyear) %>%
  group_by(RID, examyear) %>%
  fill(HMHYPERT, HMSTROKE, HMSCORE, .direction="downup") %>% # in case there are multiple rows of the same RID and exam year, impute any missing values by propagating "closest" available value
  ungroup() %>%
  distinct(RID, examyear, .keep_all=T) # keep only one row per participant-year

adni.vasc <- adni.medhist %>%
  full_join(adni.recmhist) %>%
  full_join(adni.reccmeds) %>%
  full_join(adni.modhach) %>%
  full_join(adni.vitals) %>%
  full_join(adni.av45vitals) %>%
  full_join(adni.petmeta) %>%
  full_join(adni.lipids) %>%
  left_join(adni.demog) %>%
  drop_na(RID, sex, examyear, PTDOBYY) %>%
  arrange(RID, examyear) %>%
  group_by(RID) %>%
  fill(PTDOBYY, sex, .direction="downup") %>% # impute time-invariant variables (DOB year, sex) within participant by propagating
  ungroup() %>%
  mutate(age = examyear - PTDOBYY,
         sbp = case_when(!is.na(sbp) ~ sbp,
                         !is.na(POSTSYSTBP) ~ POSTSYSTBP,
                         !is.na(PRESYSTBP) ~ PRESYSTBP),
         dbp = case_when(!is.na(dbp) ~ dbp,
                         !is.na(POSTDIABP) ~ POSTDIABP,
                         !is.na(PREDIABP) ~ PREDIABP),
         htn.consensus.how = case_when(!is.na(HMHYPERT) & (HMHYPERT==1) ~ "HMHYPERT",
                                       !is.na(htn.medhist) & (htn.medhist==1) ~ "MEDHIST",
                                       !is.na(htn.recmhist) & (htn.recmhist==1) ~ "RECMHIST",
                                       !is.na(htn.reccmeds) & (htn.reccmeds==1) ~ "RECCMEDS",
                                       !is.na(HMHYPERT) & (HMHYPERT==0) ~ "HMHYPERT",
                                       !is.na(htn.medhist) & (htn.medhist==0) ~ "MEDHIST",
                                       !is.na(htn.recmhist) & (htn.recmhist==0) ~ "RECMHIST"),
         htn.consensus = case_when(!is.na(HMHYPERT) & (HMHYPERT==1) ~ 1L, # if any of the medical history variables indicates having HTN or medication use is reported for HTN, then assign HTN dx
                                   !is.na(htn.medhist) & (htn.medhist==1) ~ 1L,
                                   !is.na(htn.recmhist) & (htn.recmhist==1) ~ 1L,
                                   !is.na(htn.reccmeds) & (htn.reccmeds==1) ~ 1L,
                                   (!is.na(HMHYPERT) & (HMHYPERT==0)) | 
                                     (!is.na(htn.medhist) & (htn.medhist==0)) | 
                                     (!is.na(htn.recmhist) & (htn.recmhist==0)) ~ 0L), # else: if any of the medical history variables indicates not having HTN, then assign not HTN
         htn.reccmeds.how = case_when(!is.na(htn.reccmeds) ~ "RECCMEDS"),
         diabetes.consensus.how = case_when(!is.na(diabetes.medhist) & (diabetes.medhist==1) ~ "MEDHIST", # if any of the medical history variables indicates having diabetes or medication use is reported for diabetes, then assign diabetes dx
                                            !is.na(diabetes.recmhist) & (diabetes.recmhist==1) ~ "RECMHIST",
                                            !is.na(diabetes.reccmeds) & (diabetes.reccmeds==1) ~ "RECCMEDS",
                                            !is.na(diabetes.medhist) & (diabetes.medhist==0) ~ "MEDHIST",
                                            !is.na(diabetes.recmhist) & (diabetes.recmhist==0) ~ "RECMHIST"),
         diabetes.consensus = case_when(!is.na(diabetes.medhist) & (diabetes.medhist==1) ~ 1L, # if any of the medical history variables indicates having diabetes or medication use is reported for diabetes, then assign diabetes dx
                                        !is.na(diabetes.recmhist) & (diabetes.recmhist==1) ~ 1L,
                                        !is.na(diabetes.reccmeds) & (diabetes.reccmeds==1) ~ 1L,
                                        (!is.na(diabetes.medhist) & (diabetes.medhist==0)) | 
                                          (!is.na(diabetes.recmhist) & (diabetes.recmhist==0)) ~ 0L), # else: if any of the medical history variables indicates not having diabetes, then assign not diabetes
         smoking.consensus.how = case_when(!is.na(smoking) & (smoking==1) ~ "MH16SMOK",
                                           !is.na(smoking.medhist) & (smoking.medhist==1) ~ "MEDHIST",
                                           !is.na(smoking.recmhist) & (smoking.recmhist==1) ~ "RECMHIST",
                                           !is.na(smoking) & (smoking==0) ~ "MH16SMOK"),
         smoking.consensus = case_when(!is.na(smoking) & (smoking==1) ~ 1L,
                                       !is.na(smoking.medhist) & (smoking.medhist==1) ~ 1L,
                                       !is.na(smoking.recmhist) & (smoking.recmhist==1) ~ 1L,
                                       !is.na(smoking) & (smoking==0) ~ 0L)) %>%
  arrange(RID, examyear) # %>%

# propagate not HTN, no HTN med use, and not diabetes backward
# propagate HTN, HTN med use, diabetes, and smoking forward
subj.list <- unique(adni.vasc$RID)
for (subj in subj.list) {
  idx <- adni.vasc$RID==subj
  
  if (sum(idx) > 1) {
    these_htn <- adni.vasc$htn.consensus[idx]
    idx.htn.missing <- is.na(these_htn)
    idx.htn0 <- !idx.htn.missing & (these_htn==0)
    idx.htn1 <- !idx.htn.missing & (these_htn==1)
    if (any(idx.htn.missing)) {
      if (any(idx.htn0)) {
        year.lasthtn0 <- max(adni.vasc$examyear[idx][idx.htn0])
        set.to.0 <- adni.vasc$examyear[idx][idx.htn.missing] < year.lasthtn0
        if (any(set.to.0)) {
          adni.vasc$htn.consensus[idx][idx.htn.missing][set.to.0] <- 0L
          adni.vasc$htn.consensus.how[idx][idx.htn.missing][set.to.0] <- "prop back 1st pass"
        }
      }
      if (any(idx.htn1)) {
        year.firsthtn1 <- min(adni.vasc$examyear[idx][idx.htn1])
        set.to.1 <- adni.vasc$examyear[idx][idx.htn.missing] > year.firsthtn1
        if (any(set.to.1)) {
          adni.vasc$htn.consensus[idx][idx.htn.missing][set.to.1] <- 1L
          adni.vasc$htn.consensus.how[idx][idx.htn.missing][set.to.1] <- "prop forw 1st pass"
        }
      }
    }
    
    these_htn.reccmeds <- adni.vasc$htn.reccmeds[idx]
    idx.htn.reccmeds.missing <- is.na(these_htn.reccmeds)
    idx.htn.reccmeds0 <- !idx.htn.reccmeds.missing & (these_htn.reccmeds==0)
    idx.htn.reccmeds1 <- !idx.htn.reccmeds.missing & (these_htn.reccmeds==1)
    if (any(idx.htn.reccmeds.missing)) {
      if (any(idx.htn.reccmeds0)) {
        year.lasthtn.reccmeds0 <- max(adni.vasc$examyear[idx][idx.htn.reccmeds0])
        set.to.0 <- adni.vasc$examyear[idx][idx.htn.reccmeds.missing] < year.lasthtn.reccmeds0
        if (any(set.to.0)) {
          adni.vasc$htn.reccmeds[idx][idx.htn.reccmeds.missing][set.to.0] <- 0L
          adni.vasc$htn.reccmeds.how[idx][idx.htn.reccmeds.missing][set.to.0] <- "prop back 1st pass"
        }
      }
      if (any(idx.htn.reccmeds1)) {
        year.firsthtn.reccmeds1 <- min(adni.vasc$examyear[idx][idx.htn.reccmeds1])
        set.to.1 <- adni.vasc$examyear[idx][idx.htn.reccmeds.missing] > year.firsthtn.reccmeds1
        if (any(set.to.1)) {
          adni.vasc$htn.reccmeds[idx][idx.htn.reccmeds.missing][set.to.1] <- 1L
          adni.vasc$htn.reccmeds.how[idx][idx.htn.reccmeds.missing][set.to.1] <- "prop forw 1st pass"
        }
      }
    }
    
    these_diabetes <- adni.vasc$diabetes.consensus[idx]
    idx.diabetes.missing <- is.na(these_diabetes)
    idx.diabetes0 <- !idx.diabetes.missing & (these_diabetes==0)
    idx.diabetes1 <- !idx.diabetes.missing & (these_diabetes==1)
    if (any(idx.diabetes.missing)) {
      if (any(idx.diabetes0)) {
        year.lastdiabetes0 <- max(adni.vasc$examyear[idx][idx.diabetes0])
        set.to.0 <- adni.vasc$examyear[idx][idx.diabetes.missing] < year.lastdiabetes0
        if (any(set.to.0)) {
          adni.vasc$diabetes.consensus[idx][idx.diabetes.missing][set.to.0] <- 0L
          adni.vasc$diabetes.consensus.how[idx][idx.diabetes.missing][set.to.0] <- "prop back 1st pass"
        }
      }
      if (any(idx.diabetes1)) {
        year.firstdiabetes1 <- min(adni.vasc$examyear[idx][idx.diabetes1])
        set.to.1 <- adni.vasc$examyear[idx][idx.diabetes.missing] > year.firstdiabetes1
        if (any(set.to.1)) {
          adni.vasc$diabetes.consensus[idx][idx.diabetes.missing][set.to.1] <- 1L
          adni.vasc$diabetes.consensus.how[idx][idx.diabetes.missing][set.to.1] <- "prop forw 1st pass"
        }
      }
    }
    
    these_smoking <- adni.vasc$smoking.consensus[idx]
    idx.smoking.missing <- is.na(these_smoking)
    idx.smoking1 <- !idx.smoking.missing & (these_smoking==1)
    if (any(idx.smoking.missing)) {
      if (any(idx.smoking1)) {
        year.firstsmoking1 <- min(adni.vasc$examyear[idx][idx.smoking1])
        set.to.1 <- adni.vasc$examyear[idx][idx.smoking.missing] > year.firstsmoking1
        if (any(set.to.1)) {
          adni.vasc$smoking.consensus[idx][idx.smoking.missing][set.to.1] <- 1L
          adni.vasc$smoking.consensus.how[idx][idx.smoking.missing][set.to.1] <- "prop forw 1st pass"
        }
      }
    }
  }
}

adni.vasc <- adni.vasc %>%
  mutate(htn.consensus.how = case_when(!is.na(htn.consensus.how) ~ htn.consensus.how,
                                       !is.na(sbp) & (sbp >= 130) ~ "SBP",
                                       !is.na(dbp) & (dbp >= 80) ~ "DBP"),
         htn.consensus = case_when(!is.na(htn.consensus) ~ htn.consensus,
                                   !is.na(sbp) & (sbp >= 130) ~ 1L,
                                   !is.na(dbp) & (dbp >= 80) ~ 1L),
         diabetes.consensus.how = case_when(!is.na(diabetes.consensus.how) ~ diabetes.consensus.how,
                                            !is.na(PMBLGLUC) & (PMBLGLUC > 125) ~ "PMBLGLUC"),
         diabetes.consensus = case_when(!is.na(diabetes.consensus) ~ diabetes.consensus,
                                        !is.na(PMBLGLUC) & (PMBLGLUC > 125) ~ 1L))


for (subj in subj.list) {
  idx <- adni.vasc$RID==subj
  
  if (sum(idx) > 1) {
    these_htn <- adni.vasc$htn.consensus[idx]
    idx.htn.missing <- is.na(these_htn)
    idx.htn1 <- !idx.htn.missing & (these_htn==1)
    if (any(idx.htn.missing)) {
      if (any(idx.htn1)) {
        year.firsthtn1 <- min(adni.vasc$examyear[idx][idx.htn1])
        set.to.1 <- adni.vasc$examyear[idx][idx.htn.missing] > year.firsthtn1
        if (any(set.to.1)) {
          adni.vasc$htn.consensus[idx][idx.htn.missing][set.to.1] <- 1L
          adni.vasc$htn.consensus.how[idx][idx.htn.missing][set.to.1] <- "prop forw 2nd pass"
        }
      }
    }
    
    these_diabetes <- adni.vasc$diabetes.consensus[idx]
    idx.diabetes.missing <- is.na(these_diabetes)
    idx.diabetes1 <- !idx.diabetes.missing & (these_diabetes==1)
    if (any(idx.diabetes.missing)) {
      if (any(idx.diabetes1)) {
        year.firstdiabetes1 <- min(adni.vasc$examyear[idx][idx.diabetes1])
        set.to.1 <- adni.vasc$examyear[idx][idx.diabetes.missing] > year.firstdiabetes1
        if (any(set.to.1)) {
          adni.vasc$diabetes.consensus[idx][idx.diabetes.missing][set.to.1] <- 1L
          adni.vasc$diabetes.consensus.how[idx][idx.diabetes.missing][set.to.1] <- "prop forw 2nd pass"
        }
      }
    }
  }
}

adni.vasc.cs <- adni.vasc %>%
  drop_na(TOTAL_C, HDL_C, sbp) %>%
  arrange(RID, age) %>%
  distinct(RID, .keep_all=T)

impute_value_TOTAL_C <- mean(adni.vasc.cs$TOTAL_C)
impute_value_HDL_C <- mean(adni.vasc.cs$HDL_C)
impute_value_sbp <- mean(adni.vasc.cs$sbp)

adni.vasc <- adni.vasc %>%
  group_by(RID) %>%
  mutate(htn.reccmeds.how = case_when(!is.na(htn.reccmeds.how) ~ htn.reccmeds.how,
                                      any(!is.na(htn.reccmeds)) ~ "downup"),
         htn.consensus.how = case_when(!is.na(htn.consensus.how) ~ htn.consensus.how,
                                       any(!is.na(htn.consensus)) ~ "downup"),
         diabetes.consensus.how = case_when(!is.na(diabetes.consensus.how) ~ diabetes.consensus.how,
                                            any(!is.na(diabetes.consensus)) ~ "downup"),
         smoking.consensus.how = case_when(!is.na(smoking.consensus.how) ~ smoking.consensus.how,
                                           any(!is.na(smoking.consensus)) ~  "downup")) %>%
  fill(TOTAL_C, LDL_C, HDL_C, TOTAL_TG, sbp, dbp, PMBLGLUC, bmi, alcohol_abuse, htn.consensus, htn.reccmeds, diabetes.consensus, smoking.consensus, .direction="downup") %>%
  ungroup() %>%
  mutate(TOTAL_C.imputed = TOTAL_C,
         HDL_C.imputed = HDL_C,
         sbp.imputed = sbp,
         htn.reccmeds.how = case_when(!is.na(htn.reccmeds.how) ~ htn.reccmeds.how,
                                      is.na(htn.reccmeds) ~ "impute 0"),
         htn.consensus.how = case_when(!is.na(htn.consensus.how) ~ htn.consensus.how,
                                       is.na(htn.consensus) ~ "impute 0"),
         diabetes.consensus.how = case_when(!is.na(diabetes.consensus.how) ~ diabetes.consensus.how,
                                            is.na(diabetes.consensus) ~ "impute 0"),
         smoking.consensus.how = case_when(!is.na(smoking.consensus.how) ~ smoking.consensus.how,
                                           is.na(smoking.consensus) ~ "impute 0")) %>%
  replace_na(list(
    htn.reccmeds = 0,
    htn.consensus = 0,
    diabetes.consensus = 0,
    smoking.consensus = 0)) %>% # assume not present if it hasn't been picked up by any of the above
  replace_na(list(TOTAL_C.imputed = impute_value_TOTAL_C, # impute continuous variables that go into the 10-yr CVD risk calculation at their mean values in the sample (just for the purpose of CVD risk calculation)
                  HDL_C.imputed = impute_value_HDL_C,
                  sbp.imputed = impute_value_sbp)) %>%
  rename(LDL = LDL_C, # rename to be consistent with BLSA variable names (for easy reporting in tables)
         HDL = HDL_C,
         CHOL = TOTAL_C,
         TRIG = TOTAL_TG,
         anthtn.med = htn.reccmeds,
         smkq = smoking.consensus,
         diabetes = diabetes.consensus) %>%
  mutate(
    cvd.risk.eq = case_when(
      sex=="Female" ~ 1 - 0.95012^(exp(2.32888 * log(age) + 
                                         1.20904 * log(TOTAL_C.imputed) +
                                         -0.70833 * log(HDL_C.imputed) +
                                         ifelse(anthtn.med==0, 2.76157, 2.82263) * log(sbp.imputed) +
                                         0.52873 * smkq + 
                                         0.69154 * diabetes - 26.1931)),
      sex=="Male" ~ 1 - 0.88936^(exp(3.06117 * log(age) + 
                                       1.12370 * log(TOTAL_C.imputed) +
                                       -0.93263 * log(HDL_C.imputed) +
                                       ifelse(anthtn.med==0, 1.93303, 1.99881) * log(sbp.imputed) +
                                       0.65451 * smkq + 
                                       0.57367 * diabetes - 23.9802))
      )
    ) %>%
  filter(age>=50)

write_csv(adni.vasc, 
          file.path(repopath,'output','adni_vascular.csv'), na="")
